<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Models\Product;
use Dotenv\Validator;
use Exception;
use Illuminate\Support\Facades\DB;

class ProductController extends BaseController
{

    public function show($point_id)
    {
        if (!is_numeric($point_id)) {
            return $this->sendError("point_id isn't valid", [], 400);
        }
        
        return $this->sendResponse(Product::getProduct($point_id), 'Product retrieved successfully.');
    }
    
}
