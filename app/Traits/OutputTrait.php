<?php

namespace App\Traits;

trait OutputTrait
{
    public static function createOutput(array $array)
    {
        $data = [];
        foreach ($array as $item) {
            $data[] = [
                'id' => $item->product_id,
                'name' =>  $item->name,
                'price' => $item->price,
                'category_id' => $item->category_id,
            ];
        }
        return $data;
    }
    public static function createOutputNew(array $array)
    {
        $data = [];
        foreach ($array as $item) {
            $data[$item->category_id]['category_id'] = $item->category_id;
            $data[$item->category_id]['name'] = $item->category_name;
            $data[$item->category_id]['items'][$item->product_id] = [
                'id' => $item->product_id,
                'name' =>  $item->name,
                'price' => $item->price,
            ];
        }
        return $data;
    }
}
