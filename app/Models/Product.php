<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Traits\OutputTrait;

class Product extends Model
{
    use OutputTrait;

    public static function getProduct($point_id)
    {
        $products = DB::select('SELECT p.*, c.name as category_name, p2sp.price, s.shop_id FROM `product` as p JOIN category as c on (p.category_id = c.category_id) JOIN product_to_shop as p2s on (p.product_id = p2s.product_id) JOIN shop as s on (p2s.shop_id = s.shop_id) JOIN product_to_shop_price as p2sp on (p2s.id = p2sp.id) WHERE s.shop_id = ?', [$point_id]);
        
        return OutputTrait::createOutput($products);
    }
}
