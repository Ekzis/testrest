<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductToShopPriceTable extends Migration
{
    public function up()
    {
        Schema::create('product_to_shop_price', function (Blueprint $table) {

		$table->increments('id');
		$table->integer('price',);

        });
    }

    public function down()
    {
        Schema::dropIfExists('product_to_shop_price');
    }
}