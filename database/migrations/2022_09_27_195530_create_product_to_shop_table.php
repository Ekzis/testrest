<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductToShopTable extends Migration
{
    public function up()
    {
        Schema::create('product_to_shop', function (Blueprint $table) {

		$table->increments('id');
		$table->integer('product_id',);
		$table->integer('shop_id',);

        });
    }

    public function down()
    {
        Schema::dropIfExists('product_to_shop');
    }
}