<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopTable extends Migration
{
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {

		$table->increments('shop_id');

        });
    }

    public function down()
    {
        Schema::dropIfExists('shop');
    }
}