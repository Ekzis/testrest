<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {

		$table->increments('product_id');
		$table->integer('category_id');
		$table->string('name',128);

        });
    }

    public function down()
    {
        Schema::dropIfExists('product');
    }
}