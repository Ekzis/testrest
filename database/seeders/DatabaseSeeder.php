<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $k = 1;
        for($i=1; $i <= 5; $i++) {
            DB::table('product')->insert([
                'product_id' => $i,
                'category_id' => random_int(1,2),
                'name' => 'product ' . $i,
            ]);

            for($j=1; $j <= 3; $j++) {
                DB::table('product_to_shop_price')->insert([
                    'id' => $k,
                    'price' => random_int(100, 400),
                ]);
                DB::table('product_to_shop')->insert([
                    'id' => $k,
                    'product_id' => $i,
                    'shop_id' => $j,
                ]);
                $k++;
            }

        }
        
        for($i=1; $i <= 3; $i++) {
            DB::table('shop')->insert(['shop_id' => $i]);
        }

        for($i=1; $i <= 2; $i++) {
            DB::table('category')->insert([
                'category_id' => $i,
                'name' => 'category ' . $i,
            ]);
        }
        
    }
}
